/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fortinetkafka;

import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.BlockingQueue;

import com.mycompany.packetprocessors.CtrlIdProcessor;
import com.mycompany.packetprocessors.CtrlStaProcessor;
import com.mycompany.packetprocessors.FapIdProcessor;

/**
 *
 * @author hp
 */
public class DataProcessor implements Runnable{
    
    private final BlockingQueue<DatagramPacket> messageQueue;
        
	public DataProcessor(BlockingQueue<DatagramPacket> messageQueue) {
		this.messageQueue = messageQueue;
	}

	@Override
	public void run() {
            System.out.println("Reached run method in DataProcessor class");
		while (Thread.currentThread().isAlive()) {
			try {
				DatagramPacket datagramPacket = this.messageQueue.take();
                                System.out.println("datagramPacket in the DataProcessor class"+datagramPacket);
                                // bytebuffer is implemented in Java NIO api which is an alternative io API. java nio enables the user to perform non-blocking io where
                                // a thread can ask a channel to read data into the buffer and while a channel reads data into the buffer a thread can do something else.
				ByteBuffer bb = ByteBuffer.allocate(datagramPacket.getLength());
                                //allocating a ByteBuffer with a capacity equal to the datagramPacket length
				bb.put(datagramPacket.getData(), 0, datagramPacket.getLength());
                                
				//Need to flip the buffer to prepare to work with it.
				bb.flip();
                                // After writing into the buffer the buffer needs to be flipped to read data from it. 
				// Check the type of message, then palm off to a separate process to deal with
				// the packet type individually
				byte[] messageType = new byte[8];
				bb.get(messageType, 0, messageType.length);
				String messageTypeStr = new String(messageType, StandardCharsets.UTF_8);
                                System.out.println("The messagetypeStr is"+messageTypeStr);
				if (messageTypeStr.contains("CTRL_ID")) {
                                        System.out.println("Called the process method of CtrlIdProcessor from DataProcessor");
					new CtrlIdProcessor(bb).process();
				} else if (messageTypeStr.contains("CTRL_STA")) {
                                        System.out.println("Called the process method of CtrlStaProcessor from DataProcessor");
					new CtrlStaProcessor(bb).process();
				} else if (messageTypeStr.contains("CTRL_ROG")) {
					//TODO: Implement
				} else if (messageTypeStr.contains("FAP_ID")) {
                                        System.out.println("Called the process method of CtrlFapProcessor from DataProcessor");
					new FapIdProcessor(bb).process();
				} else {
					System.out.println("UNKNOWN PACKET TYPE!!! " + messageTypeStr);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
