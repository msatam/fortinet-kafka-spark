/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fortinetkafka;

import java.net.DatagramPacket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author hp
 */
public class Main {
   
    
    public static void main(String[] args) {
		int port = 300;
		
		//Change queue size if needed
		BlockingQueue<DatagramPacket> messageQueue = new ArrayBlockingQueue<>(1200);
                //blocking queue can be used by multiple threads to add or remove values. It blocks a thread when it tries to dequeue from an empty queue and vice versa.
		UdpClient client = new UdpClient(port, messageQueue);
		DataProcessor dataProcessor = new DataProcessor(messageQueue);
		
		//Use 2 Threads
		//Can just increase threadpool and add more data processors as needed (repeat line 28)
		ExecutorService executorService = Executors.newFixedThreadPool(4);
                //implementation of ExecutorService implementation is very similar to a threadpool implementation
                //creating a pool of 2 threads 
		executorService.submit(client);
                //submit() submits a Callable or Runnable task to an ExecutorService and returns a result of type Future.
		executorService.submit(dataProcessor);
	}
    
}
