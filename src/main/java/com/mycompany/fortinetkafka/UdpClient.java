/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fortinetkafka;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.BlockingQueue;

/**
 *
 * @author hp
 */
public class UdpClient implements Runnable{
    
    private final int port;
	private final BlockingQueue<DatagramPacket> messageQueue;

	public UdpClient(int port, BlockingQueue<DatagramPacket> messageQueue) {
		this.port = port;
		this.messageQueue = messageQueue;
                System.out.println("UDP client reached");
	}

	@Override
	public void run() {
            
                
		try (DatagramSocket socket = new DatagramSocket(port)) {
                    
			while (Thread.currentThread().isAlive()) {
				// Max UDP packet size is 65507 bytes. 
				// We know from Fortinet stuff we'll never exceed 2048 (based on their API doc)
				byte[] buffer = new byte[2048];
                                //byte buffer is the data that has to be sent in a UDP packet. 2048 bytes is the maximum amount of data that can be sent in a single UDP packet
				DatagramPacket datagramPacket = new DatagramPacket(buffer, 0, buffer.length);
                                //datagram packets are used to implement a connectionless packet delivery service.
                                //System.out.println("The datagram packet is:"+datagramPacket);
				socket.receive(datagramPacket);
				this.messageQueue.put(datagramPacket);
                                System.out.println("The datagram packet received in socket.receive() is:"+datagramPacket);
                                //System.out.println("datagram packet put in the message queue");
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
    
}
