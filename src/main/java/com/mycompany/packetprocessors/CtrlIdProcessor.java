/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packetprocessors;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
//import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import com.mycompany.packets.CtrlId;

/**
 *
 * @author hp
 */
public class CtrlIdProcessor {

    /*
     * CTRL_ID packets:
     * -96 byte header
     * -184 byte FAP_ID__ packets (double underscore at the end)
     * -20 byte HMAC-SHA1 signature
     * 
     * It is unclear as to whether FAP_ID packets can come through independently, so they
     * have been split into their own processor.
     * 
     * The processor will check for additional bytes and then try and parse a FAP_ID packet
     */
    private final ByteBuffer bb;
    //the nio package consists of channels, buffers and selectors. One of the implementations of buffer is the Bytebuffer
    private final CtrlId ctrlId;
    //String brokers = "1000";

    public CtrlIdProcessor(ByteBuffer bb) {
        this.bb = bb;
        this.ctrlId = new CtrlId();
    }

    public void process() throws InterruptedException {
        
        System.out.println("Reached the process() method in CtrlIdProcessor");
        ctrlId.setApiVersion(bb.getShort());
        ctrlId.setControllerSerial(processStringFromByte(18));
        ctrlId.setControllerName(processStringFromByte(24));
        ctrlId.setControllerOsVersion(processStringFromByte(26));
        ctrlId.setVdomName(processStringFromByte(18));
        
                
    }

    private String processStringFromByte(int sizeInBytes) {
        byte[] buffer = new byte[sizeInBytes];
        bb.get(buffer, 0, sizeInBytes);
        return new String(buffer, StandardCharsets.UTF_8);
    }

}
