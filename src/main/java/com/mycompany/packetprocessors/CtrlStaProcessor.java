/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packetprocessors;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.mycompany.packets.CtrlSta;
import com.mycompany.packets.FapSta;
import com.mycompany.packets.StaLocate;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import com.mycompany.fortinetkafka.Main;

/**
 *
 * @author hp
 */
public class CtrlStaProcessor {

    /*
     * CTRL_STA Packets: -28 byte header -39 byte FAP_STA header -40 byte STA_LOCATE
     * packets 1..40 packets (60 bytes with 20 byte HMAC signature)
     * 
     * Process CTRL_STA packet header followed by FAP_STA header then 1..40
     * STA_LOCATE pieces FAP_STA has a number of messages indicator that can be
     * used, otherwise calculate number of messages to process based on the overall
     * packet size.
     */
    private final ByteBuffer bb;
    private final CtrlSta ctrlSta;
    private final FapSta fapSta;

    String topicName = "first";

    private final char[] hexArray = "0123456789ABCDEF".toCharArray();

    public CtrlStaProcessor(ByteBuffer bb) {
        this.bb = bb;
        this.fapSta = new FapSta();
        this.ctrlSta = new CtrlSta(fapSta);
    }

    public void process() {

        ctrlSta.setApiVersion(bb.getShort());
        ctrlSta.setControllerSerial(processStringFromByte(18));

        fapSta.setMessageType(processStringFromByte(8));
        fapSta.setApiVersion(bb.getShort());
        fapSta.setSequence(bb.getShort());
        fapSta.setNumberOfMessages(bb.get());

        fapSta.setFapSerial(processStringFromByte(18));
        fapSta.setApMac(processMacAddress());

	// There's a 2 byte padding before the start of the STA_LOCATE packets, move the
        // buffer forward
        bb.get();
        bb.get();
        // Could use bb.position(new int) instead...

        StaLocate staLocate;
        List<StaLocate> staLocateList = new ArrayList<>();

        // Process all the staLocate messages
        for (int i = 0; i < fapSta.getNumberOfMessages(); i++) {
	// New object
            staLocate = new StaLocate();

            staLocate.setRadioBssidMac(processMacAddress());
            staLocate.setDeviceMac(processMacAddress());
            staLocate.setBssidMac(processMacAddress());
            staLocate.setTypeOfDevice(bb.get());
            staLocate.setDataRate(bb.get());
            staLocate.setChannel(bb.get());
            staLocate.setAverageSignal(bb.get());
            staLocate.setNumberOfPackets(bb.getShort());
            staLocate.setMinSignal(bb.get());
            staLocate.setMaxSignal(bb.get());
            staLocate.setAgeOfFirstObservation(bb.getInt());
            staLocate.setAgeofLastObservation(bb.getInt());
            staLocate.setxCoordinate(bytesToHex(2));
            staLocate.setyCoordinate(bytesToHex(2));

            // 2 byte reserved
            bb.get();
            bb.get();

            staLocateList.add(staLocate);
        }
        fapSta.setStaLocateList(staLocateList);

        // 20 byte HMAC signature
        byte[] hmac = new byte[20];
        bb.get(hmac, 0, 20);

        Kafka_Producer producer = new Kafka_Producer(topicName, this.ctrlSta);
        producer.start();
        System.out.println("Creating a consumer object");
        Kafka_Consumer consumer = new Kafka_Consumer();
        System.out.println("Created a consumer object");
        consumer.start();
        
    }

    private String processStringFromByte(int sizeInBytes) {
        byte[] buffer = new byte[sizeInBytes];
        bb.get(buffer, 0, sizeInBytes);
        return new String(buffer, StandardCharsets.UTF_8);
    }

    private String processMacAddress() {
        byte[] mac = new byte[6];
        bb.get(mac, 0, 6);
        return formatMacAddressFromByte(mac);
    }

    private String formatMacAddressFromByte(byte[] macByte) {
        StringBuilder sb = new StringBuilder(18);
        for (byte b : macByte) {
            if (sb.length() > 0) {
                sb.append(":");
            }
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    private String bytesToHex(int sizeInBytes) {
        byte[] bytes = new byte[sizeInBytes];
        bb.get(bytes, 0, sizeInBytes);
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

}
