/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packetprocessors;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

import com.mycompany.packets.FapId;
import java.util.Properties;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
/**
 *
 * @author hp
 */
public class FapIdProcessor {
    
        private final ByteBuffer bb;
	private final FapId fapId;
        String topicName = "first";
	
	public FapIdProcessor(ByteBuffer bb) {
		this.bb = bb;
		this.fapId = new FapId();
	}
	
	public void process() {
		fapId.setMessageType(processStringFromByte(8));
		fapId.setApiVersion(bb.getShort());
		fapId.setProjectName(processStringFromByte(16));
		fapId.setSequence(bb.getShort());
		fapId.setMajorVersion(bb.get());
		fapId.setMinorVersion(bb.get());
		fapId.setApBoardMac1(processMacAddress());
		fapId.setApBoardMac2(processMacAddress());
		fapId.setApRadio1Mac(processMacAddress());
		fapId.setStaLocateEnabled1(bb.get());
		fapId.setApRadio2Mac(processMacAddress());
		fapId.setStaLocateEnabled2(bb.get());
		fapId.setApRadio3Mac(processMacAddress());
		fapId.setStaLocateEnabled3(bb.get());
		fapId.setApSerial(processApSerial());
		fapId.setApName(processStringFromByte(64));
		try {
			fapId.setLocalIpv4Address(processLocalIpv4Address());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		fapId.setFapOsVersion(processStringFromByte(32));
		fapId.setConnectionState(bb.get());
		fapId.setFlags(bb.get());
		
		//Reserved byte
		bb.get();
		
		//Process HMAC
		byte[] hmacSha1 = new byte[20];
		bb.get(hmacSha1, 0, 20);
		
		LocalDateTime localDateTime = LocalDateTime.now();
		String currentDts = localDateTime.toString();
                
                Properties props = new Properties();

        props.put("bootstrap.servers", "localhost:9092");

        //set acknowledgements for all producer requests
        props.put("acks", "all");

        //if the request fails the producer can automatically retry
        props.put("retries", 0);

        //specify buffer size in config
        props.put("batch.size", 16384);

        //Reduce the number of requests less than 0
        props.put("linger.ms", 1);

        props.put("buffer.memory", 33554432);

          //props.put("client.id", "KafkaProject");
        /* props.put("key.serializer", 
         "com.mycompany.packetprocessors.PacketSerializer");*/
        props.put("value.serializer",
                "com.pernix.packetprocessors.PacketSerializer");

        KafkaProducer<String,FapId> producer = new KafkaProducer<>(props);
        
        
        producer.send(new ProducerRecord<String, FapId>(topicName, this.fapId));
        System.out.println("Message " + this.fapId.toString() + " sent !!");
		
		/*DBObject document = new BasicDBObject("_id", new ObjectId())
				.append("dts", currentDts)
				.append("apiVersion", fapId.getApiVersion())
				.append("projectName", fapId.getProjectName().trim())
				.append("sequence", Short.toUnsignedInt(fapId.getSequence()))
				.append("majorVersion", fapId.getMajorVersion())
				.append("minorVersion", fapId.getMinorVersion())
				.append("apBoardMac1", fapId.getApBoardMac1().trim())
				.append("apBoardMac2", fapId.getApBoardMac2().trim())
				.append("apRadio1Mac", fapId.getApRadio1Mac().trim())
				.append("staLocateEnabled1", fapId.getStaLocateEnabled1())
				.append("apRadio2Mac", fapId.getApRadio2Mac().trim())
				.append("staLocateEnabled2", fapId.getStaLocateEnabled2())
				.append("apRadio3Mac", fapId.getApRadio3Mac().trim())
				.append("staLocateEnabled3", fapId.getStaLocateEnabled3())
				.append("apSerial", fapId.getApSerial().trim())
				.append("apName", fapId.getApName().trim())
				.append("localIpv4Address", fapId.getLocalIpv4Address().toString())
				.append("fapOsVersion", fapId.getFapOsVersion().trim())
				.append("connectionState", fapId.getConnectionState())
				.append("flags", fapId.getFlags());

		MongoContext.get().saveDocument(document, "FapId");*/
	}

	/*
	 * Returns a UTF-8 formatted string from the ByteBuffer based on the length in bytes passed
	 */
	private String processStringFromByte(int sizeInBytes) {
		byte[] buffer = new byte[sizeInBytes];
		bb.get(buffer, 0, sizeInBytes);
		return new String(buffer, StandardCharsets.UTF_8);
	}
	
	private String processMacAddress() {
		byte[] mac = new byte[6];
		bb.get(mac, 0, 6);
		return formatMacAddressFromByte(mac);
	}
	
	private String formatMacAddressFromByte(byte[] macByte) {
		StringBuilder sb = new StringBuilder(18);
		for (byte b : macByte) {
			if (sb.length() > 0) {
				sb.append(":");
			}
			sb.append(String.format("%02x",  b));
		}
		return sb.toString();
	}
	
	private Inet4Address processLocalIpv4Address() throws UnknownHostException {
		byte[] ipAddress = new byte[4];
		bb.get(ipAddress, 0, 4);
		return (Inet4Address) Inet4Address.getByAddress(ipAddress);
	}
	
	private String processApSerial() {
		byte[] serial = new byte[18];
		bb.get(serial, 0, 18);
		return new String(serial, StandardCharsets.UTF_8);
	}
    
}
