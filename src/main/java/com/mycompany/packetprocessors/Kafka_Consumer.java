/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packetprocessors;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.*;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import com.mycompany.packets.CtrlSta;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import scala.Tuple2;

/**
 *
 * @author mansi
 */
public class Kafka_Consumer extends Thread {

    
    private final String topic;
    Map<String, Object> kafkaParams = new HashMap<>();

    public Kafka_Consumer() {

        //Configure Spark to listen messages in topic first
        this.topic = "first";

    }

    @Override
    public void run() {

       /* try {
            System.out.println("Reached the run method of the consumer");
            // Start reading messages from Kafka and get DStream
            SparkConf conf = new SparkConf().setMaster("local").setAppName("Kafka_Consumer");
            
            JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(30));
            kafkaParams.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
            kafkaParams.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                    "org.apache.kafka.common.serialization.StringDeserializer");
            kafkaParams.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                    "org.apache.kafka.common.serialization.StringDeserializer");
            //kafkaParams.put(ConsumerConfig.GROUP_ID_CONFIG,"group1");
            kafkaParams.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
            kafkaParams.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
            Collection<String> topics = Arrays.asList("first");

            final JavaInputDStream<ConsumerRecord<String, CtrlSta>> stream;
            stream = KafkaUtils.createDirectStream(jssc, LocationStrategies.PreferConsistent(),
                    ConsumerStrategies.<String, CtrlSta>Subscribe(topics, kafkaParams));
            
            jssc.start();
            jssc.awaitTermination();
            
            */
            
            
            Properties props = new Properties();
            
            props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
            props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
            "org.apache.kafka.common.serialization.StringDeserializer");
            props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
            "org.apache.kafka.common.serialization.StringDeserializer");
            props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
            props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
            props.put("group.id", "test-consumer-group");
            //props.put(topic, props)
            
            try (KafkaConsumer<String, CtrlSta> consumer = new KafkaConsumer<>(props)) {
            
            System.out.println("Reached the try block of the consumer");
            
            consumer.subscribe(Arrays.asList(this.topic));
            
            while (true) {
            try {
            System.out.println("Produced a stream in the consumer");
            ConsumerRecords<String, CtrlSta> records = consumer.poll(1000);
            
            
            System.out.println("Reached the while loop of the consumer");
            for (ConsumerRecord<String, CtrlSta> record : records) {
            System.out.printf("%s [%d] offset=%d, key=%s, value=\"%s\"\n",
            record.topic(), record.partition(),
            record.offset(), record.key(), record.value());
            
            }
            } catch (Exception e) {
            e.printStackTrace();
            }
            
            }
            } catch (Exception e) {
            
            e.printStackTrace();
            }
        }
    }

