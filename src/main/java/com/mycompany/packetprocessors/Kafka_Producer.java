/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packetprocessors;

import java.util.Properties;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import com.mycompany.packets.CtrlSta;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mansi
 */
public class Kafka_Producer extends Thread{

    private final CtrlSta ctrlSta;
    private final String topic;
    Properties props = new Properties();
    KafkaProducer<String,CtrlSta> producer;

    public Kafka_Producer(String topic, CtrlSta ctrlSta) {

        props.put("bootstrap.servers", "localhost:9092");

        //set acknowledgements for all producer requests
        props.put("acks", "all");

        //if the request fails the producer can automatically retry
        props.put("retries", 0);

        //specify buffer size in config
        props.put("batch.size", 16384);

        //Reduce the number of requests less than 0
        props.put("linger.ms", 1);

        props.put("buffer.memory", 33554432);

        //props.put("client.id", "KafkaProject");
        props.put("key.serializer", "com.mycompany.packetprocessors.PacketSerializer");
        props.put("value.serializer", "com.mycompany.packetprocessors.PacketSerializer");

        this.topic = topic;
        this.ctrlSta = ctrlSta;
    }

    @Override
    public void run() {

        System.out.println("Reached the run method in kafka producer");

        System.out.println("Created a KafkaProducer object");
        producer = new KafkaProducer<>(props);
       
            for (int i = 0; i < 1; i++) {

                producer.send(new ProducerRecord<String,CtrlSta>(this.topic,this.ctrlSta));
                System.out.println("Message" + this.ctrlSta.toString() + " sent !!");
                //Thread.sleep(1000);
            }
        producer.close();  

    }

}
//.\bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic test

// 

//.\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic CtrlSta --group group1

