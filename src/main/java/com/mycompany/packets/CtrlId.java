/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packets;

/**
 *
 * @author hp
 */
public class CtrlId {
    
    private String messageType;
	private short apiVersion;
	private String controllerSerial;
	private String controllerName;
	private String controllerOsVersion;
	private String vdomName;
	
	public CtrlId() {
		//API documentation has the trailing underscore
		this.messageType = "CTRL_ID_";
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public short getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(short apiVersion) {
		this.apiVersion = apiVersion;
	}

	public String getControllerSerial() {
		return controllerSerial;
	}

	public void setControllerSerial(String controllerSerial) {
		this.controllerSerial = controllerSerial;
	}

	public String getControllerName() {
		return controllerName;
	}

	public void setControllerName(String controllerName) {
		this.controllerName = controllerName;
	}

	public String getControllerOsVersion() {
		return controllerOsVersion;
	}

	public void setControllerOsVersion(String controllerOsVersion) {
		this.controllerOsVersion = controllerOsVersion;
	}

	public String getVdomName() {
		return vdomName;
	}

	public void setVdomName(String vdomName) {
		this.vdomName = vdomName;
	}

	@Override
	public String toString() {
		return "CtrlId [messageType=" + messageType + ", apiVersion=" + apiVersion + ", controllerSerial="
				+ controllerSerial + ", controllerName=" + controllerName + ", controllerOsVersion="
				+ controllerOsVersion + ", vdomName=" + vdomName + "]";
	}
    
}
