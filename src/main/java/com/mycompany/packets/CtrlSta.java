/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packets;

/**
 *
 * @author hp
 */
public class CtrlSta {
    
    private String messageType; 
	private short apiVersion;
	private String controllerSerial;

	FapSta fapSta;
	
	public CtrlSta(FapSta fapSta) {
		this.messageType = "CTRL_STA";
		this.fapSta = fapSta;
	}

	public short getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(short apiVersion) {
		this.apiVersion = apiVersion;
	}

	public String getControllerSerial() {
		return controllerSerial;
	}

	public void setControllerSerial(String controllerSerial) {
		this.controllerSerial = controllerSerial;
	}

	public FapSta getFapSta() {
		return fapSta;
	}

	public void setFapSta(FapSta fapSta) {
		this.fapSta = fapSta;
	}

	public String getMessageType() {
		return messageType;
	}

	
	@Override
	public String toString() {
		return "CtrlSta [messageType=" + messageType + ", apiVersion=" + apiVersion + ", controllerSerial="
				+ controllerSerial + ", fapSta=" + fapSta + "]";
	}
}
