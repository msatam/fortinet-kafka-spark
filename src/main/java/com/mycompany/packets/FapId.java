/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packets;

import java.net.Inet4Address;

/**
 *
 * @author hp
 */
public class FapId {
    
    private String messageType;
	private short apiVersion;
	private String projectName;
	private short sequence;
	private byte majorVersion;
	private byte minorVersion;
	private String apBoardMac1;
	private String apBoardMac2;
	private String apRadio1Mac;
	private byte staLocateEnabled1;
	private String apRadio2Mac;
	private byte staLocateEnabled2;
	private String apRadio3Mac;
	private byte staLocateEnabled3;
	private String apSerial;
	private String apName;
	private Inet4Address localIpv4Address;
	private String fapOsVersion;
	private byte connectionState;
	private byte flags;
	
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public short getApiVersion() {
		return apiVersion;
	}
	public void setApiVersion(short apiVersion) {
		this.apiVersion = apiVersion;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public short getSequence() {
		return sequence;
	}
	public void setSequence(short sequence) {
		this.sequence = sequence;
	}
	public byte getMajorVersion() {
		return majorVersion;
	}
	public void setMajorVersion(byte majorVersion) {
		this.majorVersion = majorVersion;
	}
	public byte getMinorVersion() {
		return minorVersion;
	}
	public void setMinorVersion(byte minorVersion) {
		this.minorVersion = minorVersion;
	}
	public String getApBoardMac1() {
		return apBoardMac1;
	}
	public void setApBoardMac1(String apBoardMac1) {
		this.apBoardMac1 = apBoardMac1;
	}
	public String getApBoardMac2() {
		return apBoardMac2;
	}
	public void setApBoardMac2(String apBoardMac2) {
		this.apBoardMac2 = apBoardMac2;
	}
	public String getApRadio1Mac() {
		return apRadio1Mac;
	}
	public void setApRadio1Mac(String apRadio1Mac) {
		this.apRadio1Mac = apRadio1Mac;
	}
	public byte getStaLocateEnabled1() {
		return staLocateEnabled1;
	}
	public void setStaLocateEnabled1(byte staLocateEnabled1) {
		this.staLocateEnabled1 = staLocateEnabled1;
	}
	public String getApRadio2Mac() {
		return apRadio2Mac;
	}
	public void setApRadio2Mac(String apRadio2Mac) {
		this.apRadio2Mac = apRadio2Mac;
	}
	public byte getStaLocateEnabled2() {
		return staLocateEnabled2;
	}
	public void setStaLocateEnabled2(byte staLocateEnabled2) {
		this.staLocateEnabled2 = staLocateEnabled2;
	}
	public String getApRadio3Mac() {
		return apRadio3Mac;
	}
	public void setApRadio3Mac(String apRadio3Mac) {
		this.apRadio3Mac = apRadio3Mac;
	}
	public byte getStaLocateEnabled3() {
		return staLocateEnabled3;
	}
	public void setStaLocateEnabled3(byte staLocateEnabled3) {
		this.staLocateEnabled3 = staLocateEnabled3;
	}
	public String getApSerial() {
		return apSerial;
	}
	public void setApSerial(String apSerial) {
		this.apSerial = apSerial;
	}
	public String getApName() {
		return apName;
	}
	public void setApName(String apName) {
		this.apName = apName;
	}
	public Inet4Address getLocalIpv4Address() {
		return localIpv4Address;
	}
	public void setLocalIpv4Address(Inet4Address localIpv4Address) {
		this.localIpv4Address = localIpv4Address;
	}
	public String getFapOsVersion() {
		return fapOsVersion;
	}
	public void setFapOsVersion(String fapOsVersion) {
		this.fapOsVersion = fapOsVersion;
	}
	public byte getConnectionState() {
		return connectionState;
	}
	public void setConnectionState(byte connectionState) {
		this.connectionState = connectionState;
	}
	public byte getFlags() {
		return flags;
	}
	public void setFlags(byte flags) {
		this.flags = flags;
	}
	
	@Override
	public String toString() {
		return "FapId [messageType=" + messageType + ", apiVersion=" + apiVersion + ", projectName=" + projectName
				+ ", sequence=" + sequence + ", majorVersion=" + majorVersion + ", minorVersion=" + minorVersion
				+ ", apBoardMac1=" + apBoardMac1 + ", apBoardMac2=" + apBoardMac2 + ", apRadio1Mac=" + apRadio1Mac
				+ ", staLocateEnabled1=" + staLocateEnabled1 + ", apRadio2Mac=" + apRadio2Mac + ", staLocateEnabled2="
				+ staLocateEnabled2 + ", apRadio3Mac=" + apRadio3Mac + ", staLocateEnabled3=" + staLocateEnabled3
				+ ", apSerial=" + apSerial + ", apName=" + apName + ", localIpv4Address=" + localIpv4Address
				+ ", fapOsVersion=" + fapOsVersion + ", connectionState=" + connectionState + ", flags=" + flags + "]";
	}
    
}
