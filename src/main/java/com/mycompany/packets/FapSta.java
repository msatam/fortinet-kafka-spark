/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packets;

import java.util.List;

/**
 *
 * @author hp
 */
public class FapSta {
    
    private String messageType;
	private short apiVersion;
	private short sequence;
	private int numberOfMessages;
	private String fapSerial;
	private String apMac;
	List<StaLocate> staLocateList;

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public short getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(short apiVersion) {
		this.apiVersion = apiVersion;
	}

	public short getSequence() {
		return sequence;
	}

	public void setSequence(short sequence) {
		this.sequence = sequence;
	}

	public int getNumberOfMessages() {
		return numberOfMessages;
	}

	public void setNumberOfMessages(int numberOfMessages) {
		this.numberOfMessages = numberOfMessages;
	}

	public String getFapSerial() {
		return fapSerial;
	}

	public void setFapSerial(String fapSerial) {
		this.fapSerial = fapSerial;
	}

	public String getApMac() {
		return apMac;
	}

	public void setApMac(String apMac) {
		this.apMac = apMac;
	}

	public List<StaLocate> getStaLocateList() {
		return staLocateList;
	}

	public void setStaLocateList(List<StaLocate> staLocateList) {
		this.staLocateList = staLocateList;
	}

	@Override
	public String toString() {
		return "FapSta [messageType=" + messageType + ", apiVersion=" + apiVersion + ", sequence=" + sequence
				+ ", numberOfMessages=" + numberOfMessages + ", fapSerial=" + fapSerial + ", apMac=" + apMac
				+ ", staLocateList=" + staLocateList + "]";
	}
    
}
