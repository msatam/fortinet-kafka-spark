/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.packets;

/**
 *
 * @author hp
 */
public class StaLocate {
    
    private String radioBssidMac;
	private String deviceMac;
	private String bssidMac;
	private byte typeOfDevice;
	private byte dataRate;
	private byte channel;
	private short numberOfPackets;
	private byte averageSignal;
	private byte minSignal;
	private byte maxSignal;
	private int ageOfFirstObservation;
	private int ageofLastObservation;
	private String xCoordinate;
	private String yCoordinate;

	public String getRadioBssidMac() {
		return radioBssidMac;
	}

	public void setRadioBssidMac(String radioBssidMac) {
		this.radioBssidMac = radioBssidMac;
	}

	public String getDeviceMac() {
		return deviceMac;
	}

	public void setDeviceMac(String deviceMac) {
		this.deviceMac = deviceMac;
	}

	public byte getTypeOfDevice() {
		return typeOfDevice;
	}

	public void setTypeOfDevice(byte typeOfDevice) {
		this.typeOfDevice = typeOfDevice;
	}

	public byte getDataRate() {
		return dataRate;
	}

	public void setDataRate(byte dataRate) {
		this.dataRate = dataRate;
	}

	public byte getChannel() {
		return channel;
	}

	public void setChannel(byte channel) {
		this.channel = channel;
	}

	public short getNumberOfPackets() {
		return numberOfPackets;
	}

	public void setNumberOfPackets(short numberOfPackets) {
		this.numberOfPackets = numberOfPackets;
	}
	
	public byte getAverageSignal() {
		return averageSignal;
	}
	
	public void setAverageSignal(byte averageSignal) {
		this.averageSignal = averageSignal;
	}

	public byte getMinSignal() {
		return minSignal;
	}

	public void setMinSignal(byte minSignal) {
		this.minSignal = minSignal;
	}

	public byte getMaxSignal() {
		return maxSignal;
	}

	public void setMaxSignal(byte maxSignal) {
		this.maxSignal = maxSignal;
	}

	public int getAgeOfFirstObservation() {
		return ageOfFirstObservation;
	}

	public void setAgeOfFirstObservation(int ageOfFirstObservation) {
		this.ageOfFirstObservation = ageOfFirstObservation;
	}

	public int getAgeofLastObservation() {
		return ageofLastObservation;
	}

	public void setAgeofLastObservation(int ageofLastObservation) {
		this.ageofLastObservation = ageofLastObservation;
	}

	public String getxCoordinate() {
		return xCoordinate;
	}

	public void setxCoordinate(String xCoordinate) {
		this.xCoordinate = xCoordinate;
	}

	public String getyCoordinate() {
		return yCoordinate;
	}

	public void setyCoordinate(String yCoordinate) {
		this.yCoordinate = yCoordinate;
	}

	public String getBssidMac() {
		return bssidMac;
	}

	public void setBssidMac(String bssidMac) {
		this.bssidMac = bssidMac;
	}

	@Override
	public String toString() {
		return "StaLocate [radioBssidMac=" + radioBssidMac + ", deviceMac=" + deviceMac + ", bssidMac=" + bssidMac
				+ ", typeOfDevice=" + typeOfDevice + ", dataRate=" + dataRate + ", channel=" + channel
				+ ", numberOfPackets=" + numberOfPackets + ", averageSignal=" + averageSignal + ", minSignal="
				+ minSignal + ", maxSignal=" + maxSignal + ", ageOfFirstObservation=" + ageOfFirstObservation
				+ ", ageofLastObservation=" + ageofLastObservation + ", xCoordinate=" + xCoordinate + ", yCoordinate="
				+ yCoordinate + "]";
	}
    
}
